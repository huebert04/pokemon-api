import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { PokemonAPI, Pokemon, PokemonDetails } from '../interface';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})

export class PokemonListComponent implements OnInit {
  //@Output() exportAllPokemon = new EventEmitter<any>();
  pokemons: Array<Pokemon>;
  offset = 0;

  constructor(private pokemonService: PokemonService ) { }

  ngOnInit() {
    this.getAllPokemon();
  }

  getAllPokemon() {
    this.pokemonService.getPokemon(this.offset).subscribe((data: PokemonAPI) => {
      this.pokemons = data.results;
        this.pokemons.forEach(pokemon => {
          this.getPokemonDetails(pokemon);
        });
    });
  }

  getPokemonDetails(pokemon: Pokemon) {
    this.pokemonService
      .getPokemonDetails(pokemon.name)
      .subscribe((details: PokemonDetails) => {
        pokemon.details = details;
        // set pokemon id
        pokemon.id = pokemon.details.id;
        //MIGHT NEED THIS TO EMIT VARIABLES IN NEXT COMPONENT
        // // when last pokemon details have been loaded
        // // send pokemons to header component
        // if (pokemon.id === '151') {
        //   this.exportAllPokemon.emit(this.pokemons.results);
        // }
      });
  }

  getNextPage(): void {
    this.offset += 20;
    
    this.getAllPokemon();
  }

  /*getPrevPage(): void {
    if(this.offset != 0) {
      this.offset -= 20;
    } 
    
    this.getAllPokemon();
  }*/
}
