import { Component, OnInit, Input} from '@angular/core';
import { PokemonDetails, TypeColors } from '../interface';

@Component({
  selector: 'app-pokemon-stats',
  templateUrl: './pokemon-stats.component.html',
  styleUrls: ['./pokemon-stats.component.scss']
})
export class PokemonStatsComponent implements OnInit {
  @Input() pokemon: PokemonDetails;
  constructor() { }

  ngOnInit() {

  }

  getTypeColor(type: string): string {
    if (type) {
      return '#' + TypeColors[type];
    }
  }

}
