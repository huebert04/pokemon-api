export enum TypeColors {
  bug = "B1C12E",
  dark = "4F3A2D",
  dragon = "755EDF",
  electric = "F8D030",
  fairy = "F4B1F4",
  fighting = "82351D",
  fire = "E73B0C",
  flying = "A3B3F7",
  ghost = "6060B2",
  grass = "74C236",
  ground = "D3B357",
  ice = "A3E7FD",
  normal = "C8C4BC",
  poison = "934594",
  psychic = "ED4882",
  rock = "B9A156",
  steel = "B5B5C3",
  water = "3295F6"
}

export interface Pokemon {
  name: string;
  url: string;
  id: number;
  details: PokemonDetails;
}

export interface PokemonDetails {
  name: string;
  id: number;
  height: number;
  weight: number;
  base_experience: number;
  sprites: Sprites;
  abilities: Array<any>;
  types: Array<any>;
  moves: Array<any>;
  stats: Array<any>;
  species: Species;
}

export interface Sprites {
  front_default: string;
  back_default: string;
  front_shiny: string;
  back_shiny: string;
}

export interface Species {
  name: string;
  url: string;
}

export interface PokemonAPI {
  count: number;
  next: string;
  results: Pokemon[];
}
