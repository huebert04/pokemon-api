import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { PokemonDetails } from "../interface";
import { PokemonService } from "../pokemon.service";
import { Location } from '@angular/common';

@Component({
  selector: "app-pokemon-detail",
  templateUrl: "./pokemon-detail.component.html",
  styleUrls: ["./pokemon-detail.component.scss"]
})
export class PokemonDetailComponent implements OnInit {
  name: string
  pokemonDetails: PokemonDetails;

  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService,
    private location: Location
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(param => {
      this.name = param.get("name");
      this.pokemonService.getPokemonDetails(this.name)
      .subscribe((details: PokemonDetails) => {
          this.pokemonDetails = details;
        });
    });
  }

  goBack() {
    this.location.back();
  }
}
