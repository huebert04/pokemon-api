import { Component, OnInit, Input } from '@angular/core';
import { PokemonDetails } from '../interface';

@Component({
  selector: 'app-pokemon-profile',
  templateUrl: './pokemon-profile.component.html',
  styleUrls: ['./pokemon-profile.component.scss']
})
export class PokemonProfileComponent implements OnInit {
  @Input() pokemon: PokemonDetails;
  
  constructor() { }

  ngOnInit() {
  }

}
