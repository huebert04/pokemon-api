import { Component, OnInit, Input} from '@angular/core';
import { PokemonDetails } from "../interface";

@Component({
  selector: 'app-pokemon-moves',
  templateUrl: './pokemon-moves.component.html',
  styleUrls: ['./pokemon-moves.component.scss']
})
export class PokemonMovesComponent implements OnInit {
  @Input() pokemon: PokemonDetails;

  constructor() { }

  ngOnInit() {
  }

}
