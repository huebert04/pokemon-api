import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { PokemonAPI, PokemonDetails, Pokemon } from "./interface";

@Injectable({
  providedIn: "root"
})
export class PokemonService {
  pokeAPI: any;

  constructor(private http: HttpClient) {
    this.pokeAPI = 'https://pokeapi.co/api/v2/pokemon';
  }

  getPokemon(offset): Observable<PokemonAPI> {
    return this.http.get<PokemonAPI>(`${this.pokeAPI}/?offset=${offset}&limit=24`);
  }

  getPokemonDetails(name): Observable<PokemonDetails> {
    return this.http.get<PokemonDetails>(`${this.pokeAPI}/${name}`);
  }

}
