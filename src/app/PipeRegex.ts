import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'regex' })
export class PipeRegex implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.replace(/-/g, ' ').replace(/^./, function(x){return x.toUpperCase()});
  }
}